<?php

$api = app('Dingo\Api\Routing\Router');

$app->get('/', function () use ($app) {
    return $app->version();
});

$api->group([
    'version' => 'v1',
    'namespace' => 'App\Http\Api\V1\Controllers',
], function($api)
{
    $api->get('skills/wmll', 'SkillsController@getWmll');
    $api->post('alexa/handler', 'AlexaRequestController@handler');
});


<?php

namespace App\Http\Api\V1\Transformers;

use League\Fractal\TransformerAbstract;
use stdClass;


class WmllFieldTransformer extends TransformerAbstract
{
    public function transform( stdClass $status )
    {
        $fields = [];

        foreach($status->fields as $field)
        {
            $fields[] = [
                'name' => $field->name,
                'status' => $field->status,
                'details' => $field->details
            ];
        }

        return $fields;
    }
}
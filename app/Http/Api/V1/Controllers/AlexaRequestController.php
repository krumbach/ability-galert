<?php

namespace App\Http\Api\V1\Controllers;


use App\Alexa\Common\IntentFactory;
use Illuminate\Http\Request;
use RuntimeException;

use Alexa\Request\IntentRequest;
use Alexa\Request\LaunchRequest;
use Alexa\Request\Request as AlexaRequest;
use Alexa\Response\Response as AlexaResponse;


class AlexaRequestController extends ApiController
{
    public function handler( Request $request )
    {
        $alexaRequest = AlexaRequest::fromData($request->json()->all());

        $factory = new IntentFactory();

        $alexaResponse = null;

        if ($alexaRequest instanceof IntentRequest)
        {
            $intent = $factory->getIntent($alexaRequest->intentName);

            $alexaResponse = $intent->handle($alexaRequest);
        }
        elseif ($alexaRequest instanceof LaunchRequest)
        {
            try
            {
                $appName = $this->getAppNameFromId($alexaRequest->applicationId);

                $intent = $factory->getIntent($appName . 'HelpIntent');

                $alexaResponse = $intent->handle($alexaRequest);
            }
            catch (RuntimeException $e)
            {
                $alexaResponse = new AlexaResponse;

                $alexaResponse
                    ->respond('How can I help you?')
                    ->reprompt('What do you want to do?');
            }
        }
        else
        {
            $alexaResponse = new AlexaResponse;
        }


        return response()->json($alexaResponse->render());
    }

    private function getAppNameFromId($appId)
    {
        if ($appId === env('GROUNDSKEEPER_APP_ID'))
        {
            return 'GroundsKeeper';
        }

        throw new RuntimeException('Unknown app id ' . $appId . ' please register your app ids as environment variables');
    }
}
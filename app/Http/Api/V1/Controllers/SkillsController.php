<?php

namespace App\Http\Api\V1\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use stdClass;
use App\Http\Api\V1\Transformers\WmllFieldTransformer;
use Sunra\PhpSimple\HtmlDomParser;


class SkillsController extends ApiController
{
    public function getWmll( Request $request )
    {
        $html = HtmlDomParser::file_get_html('http://wmll.org/field-conditions/');

        $available = ['Senior Field', 'Major Field', 'Minor Field'];

        $conditions = $html->find('div.field-conditions');

        $status = new stdClass;
        $status->fields = [];

        foreach ($conditions as $element) {

            $header = $element->find('header', 0)->plaintext;

            if (in_array($header, $available)) {

                $field = new stdClass;

                $field->name = $header;
                $field->status = $element->find('span.status', 0)->plaintext;
                $field->details = str_replace(array("\n", "\t", "\r"), '', $element->find('span.details', 0)->plaintext);

                $status->fields[] = $field;
            }
        }

        return $this->response->item($status, new WmllFieldTransformer());
    }
}
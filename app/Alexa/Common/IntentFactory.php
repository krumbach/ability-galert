<?php

namespace App\Alexa\Common;

use RuntimeException;

class IntentFactory
{
    public function getIntent($intentName)
    {
        $className = '\\App\\Alexa\\Intents\\' . $intentName;

        if (class_exists($className))
        {
            $intent = new $className;
        }
        else
        {
            throw new RuntimeException('Unknown intent ' . $className);
        }

        return $intent;
    }
}
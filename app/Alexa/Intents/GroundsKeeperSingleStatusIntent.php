<?php

namespace App\Alexa\Intents;

use Alexa\Request\Request;


class GroundsKeeperSingleStatusIntent extends GroundsKeeperIntent
{
    public function handle( Request $request )
    {
        $data = $this->getConditions();

        $slots = $request->slots;

        $field_name = $slots['field'];

        $field = $data['fields'][$field_name];

        $text = 'Website last scanned ' . $data['timestamp']->diffForHumans() . '. ';

        $text .= 'The ' . $field['name'] . ' is ' . $field['status'] . '. ';

        $text .= $field['details'] . '.';

        return $this->response->respond($text)->endSession();
    }
}
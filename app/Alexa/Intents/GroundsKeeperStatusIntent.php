<?php

namespace App\Alexa\Intents;

use Alexa\Request\Request;


class GroundsKeeperStatusIntent extends GroundsKeeperIntent
{
    public function handle( Request $request)
    {
        $data = $this->getConditions();

        $text = 'Website last scanned ' . $data['timestamp']->diffForHumans() . '. ';

        foreach($data['fields'] as $field)
        {
            $text .= 'The ' . $field['name'] . ' is ' . $field['status'] . '. ';
        }

        return $this->response->respond($text)->endSession();
    }

}
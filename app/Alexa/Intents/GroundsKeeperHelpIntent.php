<?php

namespace App\Alexa\Intents;

class GroundsKeeperHelpIntent extends HelpIntent
{
    protected $examples = [
        'Tell me the status of all the fields',
        'Are the fields playable today',
        'What is the status of the senior field',
        'What is the status of the major field',
        'What is the status of the minor field',
    ];

    protected $invocationName = 'groundskeeper';
}
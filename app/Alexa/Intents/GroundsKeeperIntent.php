<?php

namespace App\Alexa\Intents;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Sunra\PhpSimple\HtmlDomParser;

abstract class GroundsKeeperIntent extends Intent
{
    protected function getConditions()
    {
        $data = null;

        $cache_key = 'field_conditions';

        if (Cache::has($cache_key))
        {
            $data = Cache::get($cache_key);
        }
        else
        {
            $html = HtmlDomParser::file_get_html('http://wmll.org/field-conditions/');

            $available = ['senior' => 'Senior Field', 'major' => 'Major Field', 'minor' => 'Minor Field'];

            $conditions = $html->find('div.field-conditions');

            $fields = [];

            foreach ($conditions as $element) {

                $header = $element->find('header', 0)->plaintext;

                if (in_array($header, $available)) {

                    $key = array_search ($header, $available);

                    $fields[$key] = [
                        'name' => $header,
                        'status' => trim($element->find('span.status', 0)->plaintext),
                        'details' => str_replace(array("\n", "\t", "\r"), '', $element->find('span.details', 0)->plaintext)
                    ];
                }
            }

            $expiresAt = Carbon::now()->addMinutes(10);

            $data = [
                'timestamp' => Carbon::now(),
                'fields' => $fields
            ];

           Cache::put($cache_key, $data, $expiresAt);
        }


        return $data;
    }
}